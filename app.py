# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 12:12:53 2019

@author: Roman
"""
import sys
import time
import os
import csv
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QThread, QTimer, pyqtSignal
import pyqtgraph as pg
# import matplotlib.pyplot as plt
import pandas as pd
# import script

class MyApp(QtWidgets.QMainWindow):
    # worker = script.Worker()
    def __init__(self):
        super(MyApp, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                'gui.ui'), self)
        self.pB_plot_pr.clicked.connect(self.init_plot)
        self.pB_plot_t.clicked.connect(self.init_plot)
        self.pB_start.clicked.connect(self.push_start_stop_button)

        self.pB_rec_new.clicked.connect(self.create_popup_window)
        self.pB_rec_load.clicked.connect(self.load_recipe)
        self.pB_rec_modify.clicked.connect(self.modify_recipe)
        
        self.plot_timer = QTimer()
        self.plot_timer.timeout.connect(self.updatePlot)
        self.moved = False
    def push_start_stop_button(self):
        if self.pB_start.text() == 'START':
            if self.recipe:
                self.create_thread()
                self.worker.start.emit(self.recipe)
                self.pB_start.setText('STOP')
            else:
                print("Select recipy before start")
        else:
            self.worker.stop.emit()
            self.pB_start.setText('START')

    def create_thread(self):
        if not self.moved:
            self.my_thread = QThread()
            self.my_thread.start()
            self.worker.moveToThread(self.my_thread)
    def load_recipe(self,*load_created):
        if not load_created[0]:
            self.recipe = QtWidgets.QFileDialog.getOpenFileName(None, 'Select file',
                                        os.path.abspath(__file__),'CSV File (*.csv)')[0]
        else:
            self.recipe = load_created[0]+'.csv'
        self.label_recipe.setText(os.path.split(self.recipe)[1])
        self.fill_text_browser(self.recipe)
    def fill_text_browser(self,file):
        self.textBrowser.clear()
        with open(file, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                self.textBrowser.append('{0}\t{1}\n'.format(row[0],row[1]))
    def modify_recipe(self):
        if not self.recipe:
            file = QtWidgets.QFileDialog.getOpenFileName(None, 'Select file',
                                           os.path.abspath(__file__),'CSV File (*.csv)')[0]
        else:
            file = self.recipe
        os.startfile(file)
    def create_popup_window(self):
        self.popup = Popup_new_recipe()
        self.popup.show()
        self.popup.return_recipe.connect(self.load_recipe)
    def init_plot(self, typ):
        sender = self.sender()
        self.win=pg.GraphicsWindow("plot")
        self.win.resize(1000,600)
        if sender.objectName() == 'pB_plot_pr':
            self.typ = 'pressure'
            left_label = 'Pressure [Pa]'
        else:
            self.typ = 'temperature'
            left_label = 'Temperature [C]'
        self.plt=self.win.addPlot(row=1, col=0, title="Plot", labels={'left': left_label, 'bottom': 'time [s]'})
        self.curve=self.plt.plot(pen='b')
        
        self.label_cords = pg.LabelItem(justify="right")
        self.win.addItem(self.label_cords)
        self.proxy = pg.SignalProxy(self.plt.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)

        self.plot_timer.start(1000)

    def mouseMoved(self,evt):
        self.mousePoint = self.plt.vb.mapSceneToView(evt[0])
        self.label_cords.setText("<span style='font-size: 8pt; color: white'> x = %0.4f, <span style='color: white'> y = %0.4f</span>" % (self.mousePoint.x(), self.mousePoint.y()))
    def updatePlot(self):
        try:
            if self.typ == 'pressure':
                self.curve.setData(self.worker.data_dict['time'], #x axis
                                   self.worker.data_dict['pressure'], #y axis
                                   pen=pg.mkPen('b', width=4))
            else:
                self.curve1 = self.plt.plot(pen='r')
                self.curve.setData(self.worker.data_dict['time'], #x axis
                                   self.worker.data_dict['temperature'], #y axis
                                   pen=pg.mkPen('b', width=4))
                self.curve1.setData(self.worker.data_dict['time'], #x axis
                                    self.worker.data_dict['target_temperature'], #y axis
                                    pen=pg.mkPen('b', width=4))
        except (KeyError, AttributeError): 
            self.plot_timer.stop()
            self.win.close()
            print("DataFrame is empty")
        if not self.win.isVisible():
            print('Plot window closed')
            self.plot_timer.stop()
class Popup_new_recipe(QtWidgets.QWidget):
    return_recipe = pyqtSignal(str)
    def __init__(self):
        super(Popup_new_recipe, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                'popup.ui'), self)
        self.pB_OK.clicked.connect(self.save_recipe)
        self.hide()
    def recipe_to_file(self, lista):
        fieldnames = ['time_c', 'temp_c']
        with open(self.lE_name.text()+'.csv', 'w', newline = '') as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t')
            writer.writeheader()
            for row in zip(*lista):
                f.write("{0}\t{1}\n".format(*row))
            f.close()
        self.return_recipe.emit(self.lE_name.text())
        self.destroy()
    def save_recipe(self):
        time = self.lE_time.text().split(',')
        temp = self.lE_temp.text().split(',')
        lista = [time,temp]
        self.recipe_to_file(lista)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())      