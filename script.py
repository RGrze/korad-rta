import board
import busio
import numpy as np
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
from simple_pid import PID
from koradserial import KoradSerial
import pandas
from scipy import interpolate
from thermocouples_reference import thermocouples
import time
import csv
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

class Worker(QObject):
    start = pyqtSignal()
    stop = pyqtSignal()

    def __init__(self):
        self.init_ADQ()
        self.init_korad_PSU()

        #thermocouple first reading:
        self.typeK = thermocouples['K']
        self.fieldnames = ('time', 'target_temp', 'real_temp',
                           'pressure', 'Voltage', 'Current')
        self.data_dict = {'time': [],
                          'pressure': [],
                          'temperature': [],
                          'target_temperature': []}
        self.stop_loop = False
    def init_recipe(self):
        #receipt initialization
        df = pandas.read_csv(recipe, sep="\t")
        targ_val = df.values
        p_time = targ_val[:, 0]
        temp = targ_val[:, 1]
        self.tot_time = p_time.max()
        self.targ_temp_func = interpolate.interp1d(p_time, temp)
    def init_PID(self):
        #PID initialization
        #first target temperature:
        self.l_t_temp = self.targ_temp_func(0)
        self.pid = PID(0.25, 0.1, 0.2, setpoint=self.l_t_temp)
        self.pid.output_limits = (0, 12)
    def init_ADQ(self):
        #ADQ initialization:
        i2c = busio.I2C(board.SCL, board.SDA)
        #temperature:
        ads_t = ADS.ADS1115(i2c, address=0x49, data_rate=860)
        ads_t.gain = 16
        self.ch_temp  = AnalogIn(ads_t, ADS.P0, ADS.P1)
        #pressure:
        ads_p = ADS.ADS1115(i2c, address=0x48, data_rate=860)
        ads_p.gain = 1
        self.ch_pressure = AnalogIn(ads_p, ADS.P0)
    def init_korad_PSU(self):

        SUP_F = KoradSerial('/dev/ttyACM0')
        SUP_S = KoradSerial('/dev/ttyACM1')
        SUP_F.over_current_protection.off()
        SUP_F.over_voltage_protection.on()
        SUP_S.over_current_protection.off()
        SUP_S.over_voltage_protection.on()
        self.ch_F = SUP_F.channels[0]
        self.ch_S = SUP_S.channels[0]
        self.ch_F.current = 5
        self.ch_S.current = 5
        self.SUP_F = SUP_F
        self.SUP_S = SUP_S
    def write_headers(self, fieldnames):
        with open('data.csv', 'w', newline = '') as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t')
            writer.writeheader()
            f.close()
    def write_data(self, fieldnames, data):
        with open('data.csv', 'a', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t')
            writer.writerow(data)
            f.close()
    def update_data_dict(self, time, target_temp, temp, press):
        self.data_dict['time'].append(time)
        self.data_dict['pressure'].append(press)
        self.data_dict['target_temperature'].append(target_temp)
        self.data_dict['temperature'].append(temp)
    @pyqtSlot()
    def stop(self):
        self.SUP_F.output.off()
        self.SUP_S.output.off()
        self.stop_loop = True
    @pyqtSlot(str)
    def run(self, recipe):
        self.stop_loop = False
        self.init_recipe(recipe)
        self.init_PID()
        self.write_headers()
        time_period = 1
        temp_table = np.empty([1, 30])
        pressure_table = np.empty([1, 30])
        s_time = time.time()
        t_w = time.time()
        t_n = 0
        #main cycle:
        while t_n < (self.tot_time - 2 * time_period):
            if self.stop_loop:
                break
            t_n = time.time() - s_time
            t_temp = self.targ_temp_func(t_n)
            if t_temp != self.l_t_temp:
                self.l_t_temp = t_temp
                self.pid.setpoint = t_temp
            time.sleep(time_period + t_w - time.time())
            for k in range(0, 30):
                temp_table[0, k] = self.ch_temp.voltage
                pressure_table[0, k] = self.ch_pressure.voltage
            r_temp = typeK.inverse_CmV(np.mean(temp_table, axis=1)*1000, Tref=23)
            pressure = 10**(np.mean(pressure_table, axis=1)*87.802-4)
            print(pressure, r_temp)
            control = self.pid(r_temp)
            t_w = time.time()
            self.ch_F.voltage = control
            self.ch_S.voltage = control
            self.SUP_F.output.on()
            self.SUP_S.output.on()
            I_r = self.ch_S.output_current + self.ch_F.output_current
            data = {'time': str(t_n), 'target_temp': str(t_temp),
                    'real_temp': str(r_temp), 'pressure': str(pressure),
                    'Voltage': str(control), 'Current': str(I_r)}
            self.write_data(self.fieldnames, data)
            self.update_data_dict(t_n, t_temp, r_temp, pressure)
